import { z } from 'zod';
import Zod from '../helpers/Zod';

export const signInSchema = z.object({
        identifier: Zod.string().optional(),
        password: Zod.string().optional(),
        rememberMe: z.boolean().optional(),
        error: z.any().optional(),
    })
    .refine(
        data => data.identifier && data.password,
        { message: 'Les identifiants renseignés sont incorrects.', path: ['error'] }
    );

// Infer type from zod form validation schema
export type TSignInSchema = z.infer<typeof signInSchema>;
