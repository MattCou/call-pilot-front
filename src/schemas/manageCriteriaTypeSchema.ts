import { z } from 'zod';
import Zod from '../helpers/Zod';

export const manageCriteriaTypeSchema = z.object({
        label: Zod.string(),
        description: Zod.string().nullish(),
        grade: Zod.number(),
    });

// Infer type from zod form validation schema
export type TManageCriteriaTypeSchema = z.infer<typeof manageCriteriaTypeSchema>;
