import { Cookies, Axios as LinkwebAxios } from '@linkweb/front-engine/helpers';
import { useAppStore } from '../store/store';
import { notifications } from '@mantine/notifications';
import type { AxiosError, AxiosResponse } from 'axios';
import type { IApiError } from '@linkweb/front-engine/interfaces';

/**
 * Class Axios.
 */
export default class Axios extends LinkwebAxios {
    static {
        // Request interceptor
        Axios.axiosInstance.interceptors.request.use(
            // Add API key if retrieved from client
            config => {
                if (config.headers) {
                     // Try to retrieve apiKey from state or instance cookie
                     const apiKey: string|null = useAppStore.getState().instanceState.apiKey ??
                     Cookies.get('instance') ??
                         null;

                    if (null !== apiKey) {
                        // If found, setup custom API key header
                        config.headers['X-Api-Key'] = apiKey;
                    }

                    // Setup custom client timezone header
                    config.headers['X-TimeZone'] = Intl.DateTimeFormat().resolvedOptions().timeZone;
                }

                return config;
            },
        );

        // Response Interceptor
        Axios.axiosInstance.interceptors.response.use(
            (response: AxiosResponse) => response.data.response,
            (error: AxiosError<IApiError>) => this.errorInterceptor(error.response!.data),
        );
    }

    /**
     * Interceptor function used to display an error toast if an API call failed.
     *
     * @param errorResponse
     */
    private static errorInterceptor(errorResponse: IApiError): Promise<IApiError>  {
        // Use mantine to create a toast
        notifications.show({
            title: 'Erreur API',
            color: 'red',
            // TODO display with origin
            message: errorResponse.errors.map(error => error.message).join('\n'),
        });

        return Promise.reject(errorResponse);
    }
}
