import { Zod as LinkwebZod } from '@linkweb/front-engine/helpers';
import type { ZodRawShape } from 'zod';

/**
 * Class Zod.
 */
export default class Zod extends LinkwebZod {
    /**
     * Errors used by zod validation functions.
     */
    public static ERRORS = {
        REQUIRED: 'Ce champs est obligatoire.',
        STRING_MIN_1: 'Ce champ doit contenir au minimum 1 caractère.',
        NUMBER_POSITIVE: 'Ce champ doit être supérieur à 0.',
        NUMBER_NOT_ZERO: 'Ce champ ne doit pas être égal à 0.',
        INVALID_EMAIL: 'Veuillez saisir un email valide.',
        INVALID_PASSWORD: "Veuillez saisir un mot de passe d'au moins 8 caractéres.",
        INVALID_PASSWORD_CONFIRMATION: 'Les mots de passe ne sont pas identiques.',
        INVALID_PHONE: 'Veuillez saisir un numéro de téléphone valide.',
        INVALID_SIRET: 'Veuillez saisir un numéro de SIRET valide.',
        INVALID_APE: 'Veuillez saisir un code APE valide.',
        INVALID_URL: 'Veuillez saisir une URL valide (https://...).',
        PAGES_MIN_1: 'Veuillez configurer au minimum une page.',
        GALLERY_MAX_90: 'La galerie photo ne peut pas excéder 90 photos.',
        TEXT_MAX_LENGTH: 'Ce champ ne peut pas contenir plus de 10 000 caractères.',
        PAYMENTS_MIN_1: 'Veuillez renseignez au minimum un paiement.'
    };

    // Simplifications of validations functions for app
    public static string = () => super.string(this.ERRORS.REQUIRED, this.ERRORS.STRING_MIN_1);
    public static number = () => super.number(this.ERRORS.REQUIRED);
    public static date = () => super.date(this.ERRORS.REQUIRED);
    public static enum = (enumData: Array<string>) => super.enum(enumData, this.ERRORS.REQUIRED);
    public static object<T extends ZodRawShape>(shape: T) {
        return super.object(shape, this.ERRORS.REQUIRED)
    }

    /**
     * Custom validation function for email.
     */
    public static email = () => this.string().email(this.ERRORS.INVALID_EMAIL);

    /**
     * Custom validation function for password.
     */
    public static password = () => this.string().min(8, this.ERRORS.INVALID_PASSWORD);

    /**
     * Custom validation function for phone.
     */
    public static phone = () => this.string().length(10, this.ERRORS.INVALID_PHONE);

    /**
     * Custom validation function for siret.
     */
    public static siret = () => this.string().length(14, this.ERRORS.INVALID_SIRET);
}
