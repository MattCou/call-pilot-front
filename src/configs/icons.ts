import {
    AiOutlineCheckCircle,
    AiOutlineCloseCircle,
} from 'react-icons/ai';
import { BsTrash } from 'react-icons/bs';
import { FaPen } from 'react-icons/fa6';

// Generalization of icons used inside the app
export const icons = {
    circleCheck: AiOutlineCheckCircle,
    circleClose: AiOutlineCloseCircle,
    filledPen: FaPen,
    outlineTrash: BsTrash,
};
