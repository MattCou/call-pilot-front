import React from 'react';
import { modals as mantineModals } from '@mantine/modals';
import { Text } from '@mantine/core';
import type { OpenConfirmModal } from '@mantine/modals/lib/context';

type TOpenConfirmModal = Omit<OpenConfirmModal, 'labels'> & {
    children: React.ReactNode,
    labels?: Partial<OpenConfirmModal['labels']>,
}

type TOpenDeletionConfirmModal = Omit<OpenConfirmModal, 'labels'> & {
    text: string,
};

// Extension of '@mantine/modals' modals object
const preModals = {
    ...mantineModals,
    openConfirmModal: ({ labels, ...payload }: TOpenConfirmModal) => mantineModals.openConfirmModal({
        size: 'lg',
        title: 'Confirmer votre action',
        labels: { confirm: 'Confirmer', cancel: 'Annuler', ...labels },
        ...payload,
    }),
    openDeletionConfirmModal: ({ text, ...payload }: TOpenDeletionConfirmModal) => mantineModals.openConfirmModal({
        size: 'lg',
        title: 'Confirmer la suppression',
        children: (
            <Text ta='center' c='var(--mantine-color-error)' fw='bold' py='lg'>
                {`Attention ! Cette action est irréversible. ${text}`}
            </Text>
        ),
        labels: { confirm: 'Supprimer', cancel: 'Annuler' },
        ...payload,
    }),
};

// Extension of '@mantine/modals' modals object
export const modals = {
    ...preModals,
};
