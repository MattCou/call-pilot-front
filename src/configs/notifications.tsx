import React from 'react';
import {
    notifications as mantineNotifications,
    type NotificationData,
} from '@mantine/notifications';
import { icons } from './icons';

// Extension of '@mantine/notifications' notifications object
export const notifications = {
    ...mantineNotifications,
    showError: (notificationData: NotificationData) => mantineNotifications.show({
        title: 'Erreur',
        color: 'red',
        icon: (<icons.circleClose />),
        ...notificationData,
    }),
    showSuccess: (notificationData: NotificationData) => mantineNotifications.show({
        title: 'Succès',
        color: 'green',
        icon: (<icons.circleCheck />),
        ...notificationData,
    }),
};
