import React from 'react';
import { createBrowserRouter, Navigate, type RouteObject } from 'react-router-dom';
import { ParentFillLoader } from '@linkweb/front-engine/components';
import AppRootManager from '../components/AppRoot/AppRootManager';
import AppRootProvider from '../components/AppRoot/AppRootProvider';
import Error404 from '../components/ErrorPage/Error404';
import Error500 from '../components/ErrorPage/Error500';
import ManageCriteriaTypes from '../components/ManageCriteriaTypes/ManageCriteriaTypes';
import Workspace from '../components/Workspace/Workspace';
import SignIn from '../components/SignIn/SignIn';

/**
 * Wrapper of `createBrowserRouter` managing app's routes depending on given parameters.
 *
 * @param isAuth
 * @param isGrantedAdmin
 * @param isGrantedManager
 */
export const createAppRouter = (isAuth: boolean|null) =>
    createBrowserRouter([
        {
            path: '',
            element: (<AppRootProvider />),
            children: [
                {
                    path: '',
                    // Prevent displaying the stack of error messages occurring in the production environment
                    errorElement: 'prod' === import.meta.env.VITE_APP_ENV ? (<Error500 />) : undefined,
                    element: (<AppRootManager />),
                    children: null === isAuth ?
                        loadingRouter
                        : isAuth ?
                            authenticatedRouter
                            : unauthenticatedRouter,
                },
            ],
        },
    ]);

// Used while the application is loading
const loadingRouter: Array<RouteObject> = [
    {
        path: '',
        element: (<ParentFillLoader h='100dvh' />),
    },
    {
        path: '*',
        element: (<ParentFillLoader h='100dvh' />),
    },
];


// Used while user is unauthenticated
const unauthenticatedRouter: Array<RouteObject> = [
    {
        path: 'sign_in',
        element: (<SignIn />),
    },
    // Root path redirects to sign in path
    {
        path: '',
        element: (<Navigate to='sign_in' replace />),
    },
    // All other paths redirect to sign in path
    {
        path: '*',
        element: (<Navigate to='sign_in' replace />),
    },
];

const authenticatedRouter: Array<RouteObject> = [
    {
        path: 'sign_in',
        element: (<Navigate to='/workspace/criteria_types' replace />),
    },
    // Root path redirects to workspace dashboard path
    {
        path: '',
        element: (<Navigate to='workspace/criteria_types' replace />),
    },
    {
        path: 'workspace',
        element: (<Workspace />),
        children: [
            {
                path: 'criteria_types',
                element: (<ManageCriteriaTypes />),
            },
        ],
    },
    // Any other unknown path goes to not found
    {
        path: '*',
        element: (<Error404 />),
    },
];
