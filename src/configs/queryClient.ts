import { QueryClient } from '@tanstack/react-query';

// Create a react-query client
export default new QueryClient({
    defaultOptions: {
        queries: {
            retry: false,
            refetchOnWindowFocus: false,
            // Queries cache time is 60 seconds
            staleTime: 60 * 1000,
        },
    },
});
