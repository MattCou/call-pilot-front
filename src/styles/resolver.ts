import type { CSSVariablesResolver } from '@mantine/core';

const resolver: CSSVariablesResolver = () => ({
    variables: {},
    light: {},
    dark: {},
});

export default resolver;
