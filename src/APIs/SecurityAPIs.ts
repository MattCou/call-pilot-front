import Axios from '../helpers/Axios';

/**
 * Class SecurityAPIs.
 */
export default class SecurityAPIs {
    /**
     * Sign in to app.
     */
    public static async signIn(identifier: string, password: string, rememberMe?: boolean|undefined) {
        return Axios.post<{ apiKey: string }>('sign_in', {
            identifier: identifier,
            password: password,
            rememberMe: rememberMe,
        });
    }
}
