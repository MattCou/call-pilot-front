import Axios from '../helpers/Axios';
import type { TQuery } from '@linkweb/front-engine/types';
import type ICriteriaType from '../interfaces/models/ICriteriaType';

/**
 * Class CriteriaTypeAPIs.
 */
export default class CriteriaTypeAPIs {
    /**
     * Retrieve criteria types.
     */
    public static async getCriteriaTypes(query?: TQuery<ICriteriaType>|undefined) {
        return Axios.get<ICriteriaType>('criteria_types', query);
    }

    /**
     * Create a new CriteriaType.
     */
    public static async postCriteriaTypes(body: Record<string, unknown>) {
        return Axios.post<ICriteriaType>('criteria_types', body);
    }

    /**
     * Change positions of CriteriaTypes.
     */
    public static async postNewCriteriaTypesOrder(body: Record<string, unknown>) {
        return Axios.post<ICriteriaType>('criteria_types/order', body);
    }

    /**
     * Update an existing CriteriaType.
     */
    public static async patchCriteriaType(id: string, body: Record<string, unknown>) {
        return Axios.patch<ICriteriaType>(`criteria_types/${id}`, body);
    }

    /**
     * Delete/Archive an existing CriteriaType.
     */
    public static async deleteCriteriaType(id: string) {
        return Axios.remove('criteria_types', id);
    }
}
