import Axios from '../helpers/Axios';
import type { IInstanceUserState } from '../store/slices/instanceSlice';

/**
 * Class UserAPIs.
 */
export default class UserAPIs {
    /**
     * Retrieve authenticated user instance data.
     */
    public static async getUserIntance() {
        return Axios.get<IInstanceUserState>('users/instance');
    }
}
