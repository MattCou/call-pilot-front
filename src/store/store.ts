import { create } from 'zustand';
import { createInstanceSlice, type IInstanceSlice } from './slices/instanceSlice';

export const useAppStore = create<IInstanceSlice>()((...args) => ({
    ...createInstanceSlice(...args),
}));

/**
 * Custom zustand selector to check if the user in instanceState has a specific role.
 */
export const useIsGranted = (role: string): boolean => {
    const instanceRoles = useAppStore(state => state.instanceState.user?.roles);

    return instanceRoles ?
        instanceRoles.includes(role)
        : false;
};
