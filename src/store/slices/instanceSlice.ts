import type { StateCreator } from 'zustand';

export interface IInstanceUserState {
    id: string,
    username: string,
    roles: Array<string>,
}

interface IInstanceState {
    isAuth: boolean|null,
    apiKey: string|null,
    windowId: string,
    user: IInstanceUserState|null,
}

const initialState: IInstanceState = {
    isAuth: null,
    apiKey: null,
    // Unique string used to differentiate multiple windows on same instance
    windowId: window.crypto.randomUUID(),
    user: null,
};

// Interface that defines the store slice
export interface IInstanceSlice {
    instanceState: IInstanceState,
    setInstanceStateIsAuth: (payload: IInstanceState['isAuth']) => void,
    setInstanceStateApiKey: (payload: IInstanceState['apiKey']) => void,
    setInstanceStateAuthUser: (user: IInstanceState['user']) => void,
    clearInstanceState: () => void,
}

export const createInstanceSlice: StateCreator<IInstanceSlice> = set => ({
    instanceState: initialState,
    setInstanceStateIsAuth: payload => set((state: IInstanceSlice) => ({
        instanceState: {
            ...state.instanceState,
            isAuth: payload,
        },
    })),
    setInstanceStateApiKey: payload => set((state: IInstanceSlice) => ({
        instanceState: {
            ...state.instanceState,
            apiKey: payload,
        },
    })),
    setInstanceStateAuthUser: payload => set((state: IInstanceSlice) => ({
        instanceState: {
            ...state.instanceState,
            user: payload,
            isAuth: true,
        },
    })),
    clearInstanceState: () => set(() => ({
        instanceState: {
            ...initialState,
            isAuth: false,
        },
    })),
});
