import React from 'react';
import { useMutation } from '@tanstack/react-query';
import { useForm } from '@linkweb/front-engine/hooks';
import { useAppStore } from '../../store/store';
import SecurityAPIs from '../../APIs/SecurityAPIs';
import { Cookies } from '@linkweb/front-engine/helpers';
import { type TSignInSchema, signInSchema } from '../../schemas/signInSchema';
import { Button, Text, Stack } from '@mantine/core';
import { FormCheckbox, FormPasswordInput, FormTextInput } from '@linkweb/front-engine/components';
import classes from './SignIn.module.css';

const SignInForm: React.FC = () => {
    // Use of react hooks
    const [errorState, setErrorState] = React.useState<string|undefined>(undefined);

    // Use of react-hook-form hook
    const { control, handleSubmit, formState: { errors } } = useForm<TSignInSchema>({
        validationSchema: signInSchema,
    });

    // Use of zustand hooks
    const setInstanceStateApiKey = useAppStore(state => state.setInstanceStateApiKey);

    // API call to sign in
    const signInCall = useMutation({
        mutationFn: (values: TSignInSchema) => SecurityAPIs.signIn(values.identifier!, values.password!, values.rememberMe),
        onSuccess: ({ data }, values) => {
            // "Remember me" set an instance cookie
            if (values.rememberMe) {
                Cookies.set('instance', data[0].apiKey);
            }

            // Instance now has an apiKey which trigger getUserIntance query (AuthManager component)
            setInstanceStateApiKey(data[0].apiKey);
        },
        onError: () => setErrorState('Les identifiants renseignés sont incorrects.'),
    });

    // Function used to submit form
    function onSubmit(values: TSignInSchema) {
        // Reset errorState
        setErrorState(undefined);
        // Call mutation to sign in
        signInCall.mutate(values);
    }

    return (
        <Stack
            component='form'
            gap='xl'
            h={{ base: '100%', sm: '600px' }}
            w={{ base: 'initial', sm: '400px' }}
            className={classes.signInForm}
            onSubmit={handleSubmit(onSubmit)}
        >
            <Stack gap='xl' mb='auto'>
                <FormTextInput
                    size='md'
                    name='identifier'
                    control={control}
                    label='Identifiant'
                    placeholder='Votre identifiant'
                />
                <FormPasswordInput
                    size='md'
                    name='password'
                    control={control}
                    label='Mot de passe'
                    placeholder='Votre mot de passe'
                />
                <FormCheckbox
                    size='md'
                    name='rememberMe'
                    control={control}
                    label='Se souvenir de moi'
                />
                {
                    (errorState || errors.error) && (
                        <Text ta='center' size='sm' c='var(--mantine-color-error)'>
                            {errorState ?? errors.error!.message as string}
                        </Text>
                    )
                }
            </Stack>
            <Button
                type='submit'
                loading={signInCall.isPending || signInCall.isSuccess}
                className={classes.signInFormButton}
            >
                {`Se connecter`}
            </Button>
        </Stack>
    );
};

export default SignInForm;
