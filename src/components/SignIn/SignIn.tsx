import React from 'react';
import { useMediaQuery } from '@mantine/hooks';
import { useMantineTheme, Center } from '@mantine/core';
import SignInForm from './SignInForm';
import classes from './SignIn.module.css';

const SignIn: React.FC = () => {
    // Use of mantine hooks
    const theme = useMantineTheme();
    const mawSmBp = useMediaQuery(`(max-width: ${theme.breakpoints.sm})`);

    return (
        <div className={classes.container}>
            {
                mawSmBp ?
                    <SignInForm />
                : (
                    <Center
                        w={{ base: '100%', md: 'fit-content' }}
                        className={classes.signInFormWrapper}
                    >
                        <SignInForm />
                    </Center>
                )
            }
        </div>
    );
};

export default SignIn;
