import React from 'react';
import { Outlet } from 'react-router-dom';
import { AppShell } from '@mantine/core';
import classes from './Workspace.module.css';

const Workspace: React.FC = () => {
    return (
        <AppShell>
            <AppShell.Main className={classes.appShellMain}>
                <Outlet />
            </AppShell.Main>
        </AppShell>
    );
};

export default Workspace;
