import React from 'react';
import { Title } from '@mantine/core';

const Error500: React.FC = () => {
    return (
        <Title>
            {`Erreur 500`}
        </Title>
    );
};

export default Error500;
