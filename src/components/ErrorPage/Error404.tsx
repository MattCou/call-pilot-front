import React from 'react';
import { Title } from '@mantine/core';

const Error404: React.FC = () => {
    return (
        <Title>
            {`Erreur 404`}
        </Title>
    );
};

export default Error404;
