import React from 'react';
import { useInvalidateMutation } from '@linkweb/front-engine/hooks';
import CriteriaTypeAPIs from '../../APIs/CriteriaTypeAPIs';
import { icons } from '../../configs/icons';
import { modals } from '../../configs/modals';
import { notifications } from '../../configs/notifications';
import { ActionIcon, Box, Group, Paper, Text } from '@mantine/core';
import { FlexDnd, ParentFillLoader } from '@linkweb/front-engine/components';
import type ICriteriaType from '../../interfaces/models/ICriteriaType';

interface IManageCriteriaTypesListingProps {
    isFetching: boolean,
    data: Array<ICriteriaType>|undefined,
    setSelectedCriteriaType: (value: ICriteriaType|undefined) => void,
    criteriaTypes: ICriteriaType[],
    setCriteriaTypes: (values: Array<ICriteriaType>) => void,
}

const ManageCriteriaTypesListing: React.FC<IManageCriteriaTypesListingProps> = ({ isFetching, data, setCriteriaTypes, ...props }) => {
    // Use of react-query hooks
    const deleteCriteriaTypeCall = useInvalidateMutation({
        invalidateQueryFilters: [{ queryKey: ['criteriaTypes'] }],
        mutationFn: (id: string) => CriteriaTypeAPIs.deleteCriteriaType(id),
        onSuccess: () => notifications.showSuccess({ message: 'Le critère a été supprimé avec succès.' }),
    });

    // useEffect when criteria types are fetched from API
    React.useEffect(() => {
        false === isFetching && undefined !== data && setCriteriaTypes(data);
    }, [isFetching, data, setCriteriaTypes]);

    // Function to move an element from a position to another
    function onElementMove(from: number, to: number) {
        const newElements = [...props.criteriaTypes];
        newElements.splice(to, 0, newElements.splice(from, 1)[0]);
        setCriteriaTypes(newElements);
    }

    return (
        <Paper
            styles={{
                root: {
                    height: '100%',
                    overflowY: 'scroll',
                },
            }}
        >
            {
                isFetching || undefined === data ? (
                    <ParentFillLoader />
                ) : (
                    <FlexDnd
                        direction='column'
                        elements={props.criteriaTypes}
                        uniqueKeyAccessor={element => element.id}
                        onMove={onElementMove}
                        elementRender={({ element, elementIndex, draggableProps, containerProps }) => (
                            <Paper
                                component={Group}
                                p='lg'
                                w='100%'
                                my='sm'
                                radius='md'
                                withBorder
                                {...draggableProps}
                                {...containerProps}
                            >
                                <Box>
                                    <Text>
                                        {`${elementIndex + 1} - ${element.label} - `}
                                        <strong>
                                            {`${element.grade} point${1 === element.grade ? '' : 's'}`}
                                        </strong>
                                    </Text>
                                    <Text fs='italic' c='dimmed'>
                                        {element.description}
                                    </Text>
                                </Box>
                                <ActionIcon
                                    ml='auto'
                                    onClick={() => {
                                        props.setSelectedCriteriaType(element)
                                    }}
                                >
                                    <icons.filledPen />
                                </ActionIcon>
                                <ActionIcon
                                    color='red'
                                    onClick={() => {
                                        modals.openDeletionConfirmModal({
                                            text: `Si le critère n'est pas utilisé il sera définitivement supprimé, sinon il sera archivé.`,
                                            onConfirm: () => deleteCriteriaTypeCall.mutate(element.id),
                                        })
                                    }}
                                >
                                    <icons.outlineTrash />
                                </ActionIcon>
                            </Paper>
                        )}
                    />
                )
            }
        </Paper>
    );
};

export default ManageCriteriaTypesListing;
