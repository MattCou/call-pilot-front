import React from 'react';
import { useQuery } from '@tanstack/react-query';
import { useInvalidateMutation } from '@linkweb/front-engine/hooks';
import CriteriaTypeAPIs from '../../APIs/CriteriaTypeAPIs';
import { notifications } from '../../configs/notifications';
import { Button, Flex, Group, Title } from '@mantine/core';
import ManageCriteriaTypesListing from './ManagaCriteriaTypesListing';
import ManageCriteriaTypesForm from './ManageCriteriaTypesForm';
import type ICriteriaType from '../../interfaces/models/ICriteriaType';
import classes from './ManageCriteriaTypes.module.css';

const ManageCriteriaTypes: React.FC = () => {
    // Use of react hooks
    const [criteriaTypes, setCriteriaTypes] = React.useState<Array<ICriteriaType>>([]);

    // Use of react hooks
    const [selectedCriteriaType, setSelectedCriteriaType] = React.useState<ICriteriaType|undefined>(undefined);

    // API call to retrieve non-archived CriteriaTypes
    const { isFetching, data } = useQuery({
        queryKey: ['criteriaTypes'],
        queryFn: () => CriteriaTypeAPIs.getCriteriaTypes({
            archivedAt: {
                eq: '',
            },
        }),
        select: response => response.data,
    });

    // API call to modifiy the order of CriteriaTypes
    const changeCriteriaTypesOrderCall = useInvalidateMutation({
        invalidateQueryFilters: [{ queryKey: ['criteriaTypes'] }],
        mutationFn: values => CriteriaTypeAPIs.postNewCriteriaTypesOrder(values),
        onSuccess: () => {
            notifications.showSuccess({ message: "L'ordre des critères a bien été mis à jour." });
        },
    });

    return (
        <Flex
            direction='column'
            gap='lg'
            className={classes.container}
        >
            <Group justify='space-between'>
                <Title>
                    {`Modifier les critères d'évalution`}
                </Title>
                <Button
                    disabled={isFetching || undefined === data}
                    onClick={() => {
                        changeCriteriaTypesOrderCall.mutate({ criteriaTypes: criteriaTypes!.map((value, index) => ({ id: value.id, position: index + 1 }))})
                    }}
                >
                    {`Confirmer l'ordre des critères`}
                </Button>
            </Group>
            <ManageCriteriaTypesListing
                data={data}
                isFetching={isFetching}
                setSelectedCriteriaType={setSelectedCriteriaType}
                criteriaTypes={criteriaTypes}
                setCriteriaTypes={setCriteriaTypes}
            />
            <ManageCriteriaTypesForm
                data={data}
                criteriaType={selectedCriteriaType}
                setSelectedCriteriaType={setSelectedCriteriaType}
            />
        </Flex>
    );
}

export default ManageCriteriaTypes;
