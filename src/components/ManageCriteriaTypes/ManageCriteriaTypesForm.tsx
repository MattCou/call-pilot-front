import React from 'react';
import { useForm, useInvalidateMutation } from '@linkweb/front-engine/hooks';
import CriteriaTypeAPIs from '../../APIs/CriteriaTypeAPIs';
import { manageCriteriaTypeSchema,  type TManageCriteriaTypeSchema } from '../../schemas/manageCriteriaTypeSchema';
import { Button, Paper, Title } from '@mantine/core';
import { FormNumberInput, FormTextInput, FormTextarea } from '@linkweb/front-engine/components';
import type ICriteriaType from '../../interfaces/models/ICriteriaType';

interface IManageCriteriaTypesFormProps {
    data: Array<ICriteriaType>|undefined,
    setSelectedCriteriaType: (value: ICriteriaType|undefined) => void,
    criteriaType?: ICriteriaType|undefined,
}

const ManageCriteriaTypesForm: React.FC<IManageCriteriaTypesFormProps> = props => {
    // Callback to reset form to an empty state
    const resetForm = () => {
        props.setSelectedCriteriaType(undefined);
        reset({ label: undefined, description: undefined, grade: undefined });
    }

    // Callback when form is submitted
    const manageCriteriaTypeCall = useInvalidateMutation({
        invalidateQueryFilters: [{ queryKey: ['criteriaTypes'] }],
        mutationFn: values =>  undefined === props.criteriaType ?
            CriteriaTypeAPIs.postCriteriaTypes(values)
            : CriteriaTypeAPIs.patchCriteriaType(props.criteriaType.id, values),
        onSuccess: resetForm,
    });

    // Use of react-hook-form hooks
    const { control, handleSubmit, reset } = useForm<TManageCriteriaTypeSchema>({
        validationSchema: manageCriteriaTypeSchema,
    });

    // Callback when form is submitted
    const onSubmit = (values: TManageCriteriaTypeSchema) => {
        undefined !== props.data && manageCriteriaTypeCall.mutate({
            ...values,
            position: undefined === props.criteriaType ?
                (0 < props.data.length ? props.data[props.data.length - 1].position + 1 : 1)
                : props.criteriaType.position,
        });
    }

    // useEffect when a criteriaType is selected to be modified
    React.useEffect(() => {
        reset(props.criteriaType);
    }, [props.criteriaType, reset]);

    return (
        <Paper
            component='form'
            onSubmit={handleSubmit(onSubmit)}
            styles={{
                root: {
                    flexShrink: 0,
                    marginTop: 'auto',
                },
            }}
        >
            <Title>
                {`${undefined === props.criteriaType ? 'Créer un' : 'Modifier le'} critère`}
            </Title>
            <FormTextInput
                name='label'
                control={control}
                label='Titre'
            />
            <FormTextarea
                name='description'
                control={control}
                label='Description'
            />
            <FormNumberInput
                name='grade'
                control={control}
                label='Barême'
            />
            <Button type='submit' mt='sm'>
                {`Valider`}
            </Button>
            {
                undefined !== props.criteriaType && (
                    <Button
                        color='red'
                        mt='sm'
                        onClick={resetForm}
                    >
                        {`Annuler`}
                    </Button>
                )
            }
        </Paper>
    );
};

export default ManageCriteriaTypesForm;
