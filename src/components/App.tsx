import React from 'react';
import dayjs from 'dayjs';
import localeData from 'dayjs/plugin/localeData';
import relativeTime from 'dayjs/plugin/relativeTime';
import duration from 'dayjs/plugin/duration';
import { dayjsFrLocale } from '@linkweb/front-engine/utils';
import { useAppStore } from '../store/store';
import { createAppRouter } from '../configs/router';
import { RouterProvider } from 'react-router-dom';
import '@linkweb/front-engine/styles.css';

// Enable some dayjs plugins & set locale
dayjs.extend(localeData);
dayjs.extend(relativeTime);
dayjs.extend(duration);
dayjs.locale('Fr', dayjsFrLocale);

const App: React.FC = () => {
    // Use of zustand hooks
    const instanceStateIsAuth = useAppStore(state => state.instanceState.isAuth);

    return (
        <RouterProvider router={createAppRouter(instanceStateIsAuth)} />
    );
};

export default App;
