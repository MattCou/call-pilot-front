import React from 'react';
import { useQueryClient, useQuery} from '@tanstack/react-query';
import { useAppStore } from '../../store/store';
import { Cookies } from '@linkweb/front-engine/helpers';
import UserAPIs from '../../APIs/UserAPIs';

// AuthManager takes actions depending on authentication state of the instance
const AuthManager: React.FC = () => {
    // Use of react-query hooks
    const queryClient = useQueryClient();

    // Use of zustand hooks
    const instanceStateIsAuth = useAppStore(state => state.instanceState.isAuth);
    const instanceStateApiKey = useAppStore(state => state.instanceState.apiKey);
    const instanceStateUser = useAppStore(state => state.instanceState.user);
    const setInstanceStateIsAuth = useAppStore(state => state.setInstanceStateIsAuth);
    const setInstanceStateApiKey = useAppStore(state => state.setInstanceStateApiKey);
    const setInstanceStateAuthUser = useAppStore(state => state.setInstanceStateAuthUser);
    const clearInstanceState = useAppStore(state => state.clearInstanceState);

    // API call to retrieve instance information from instance apiKey
    const getUserInstanceCall = useQuery({
        queryKey: ['userInstance'],
        // instanceApiKey is set & need to retrieve associated user instance data
        enabled: null !== instanceStateApiKey && null === instanceStateUser,
        /**
         * IMPORTANT! Stale time should be 0 here to always refetch data on sign in.
         * In fact, it can not relie on cache as instance could have been disconnected
         * by a ban - but the cache still has instance's user marked as verified.
         */
        staleTime: 0,
        queryFn: UserAPIs.getUserIntance,
        select: response => response.data,
    });

    // useEffect which trigger actions depending on getUserInstanceCall state
    React.useEffect(() => {
        // On success
        getUserInstanceCall.isSuccess
            // Instance is authenticated & set instance data
            && setInstanceStateAuthUser(getUserInstanceCall.data[0]);
        // On error
        getUserInstanceCall.isError
            // Instance is no longer authenticated
            && setInstanceStateIsAuth(false);
    }, [
        getUserInstanceCall.isSuccess,
        getUserInstanceCall.isError,
        getUserInstanceCall.data,
        setInstanceStateAuthUser,
        setInstanceStateIsAuth,
    ]);

    // useEffect when instance authentication is unknown
    React.useEffect(() => {
        if (null === instanceStateIsAuth) {
            // 'remember me' enabled via instance cookie
            Cookies.get('instance') ?
                // Enable getUserIntance query as we set an instanceApiKey (instance cookie value)
                setInstanceStateApiKey(Cookies.get('instance')!)
                // No 'remember me' but impersonation cookie
                : setInstanceStateIsAuth(false);
        }
    }, [instanceStateIsAuth, instanceStateApiKey, setInstanceStateIsAuth, setInstanceStateApiKey]);

    // useEffect when instance is no longer authenticated
    React.useEffect(() => {
        if (false === instanceStateIsAuth) {
            // Clears all connected caches
            queryClient.clear();
            // Remove authentication related cookies
            Cookies.remove(['instance', 'impersonation']);
            // Clear instance state
            clearInstanceState();
        }
    }, [instanceStateIsAuth, queryClient, clearInstanceState]);

    return null;
};

export default AuthManager;
