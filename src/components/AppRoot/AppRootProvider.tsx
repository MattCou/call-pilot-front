import React from 'react';
import queryClient from '../../configs/queryClient';
import theme from '../../styles/theme';
import resolver from '../../styles/resolver';
import { modalsContext } from '../../configs/modalsContext';
import { QueryClientProvider } from '@tanstack/react-query';
import { MantineProvider } from '@mantine/core';
import { ModalsProvider } from '@mantine/modals';
import { Outlet } from 'react-router-dom';

/**
 * Component at highest level of the application.
 * It allows to define all the providers of the application at the same place.
 * All components are rendered as children of this component (https://reactrouter.com/en/main/components/outlet).
 */
const AppRootProvider: React.FC = () => {
    return (
        <QueryClientProvider client={queryClient}>
            <MantineProvider
                theme={theme}
                cssVariablesResolver={resolver}
            >
                <ModalsProvider modals={modalsContext}>
                    <Outlet />
                </ModalsProvider>
            </MantineProvider>
        </QueryClientProvider>
    );
};

export default AppRootProvider;
