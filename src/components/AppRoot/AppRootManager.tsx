import React from 'react';
import { Notifications as NotificationsManager } from '@mantine/notifications';
import { Outlet } from 'react-router-dom';
import AuthManager from '../AuthManager/AuthManager';

/**
 * Component at highest level of the application.
 * It allows to define all the managers of the application at the same place.
 * All components are rendered as children of this component (https://reactrouter.com/en/main/components/outlet).
 */
const AppRootManager: React.FC = () => {
    return (
        <>
            <AuthManager />
            <NotificationsManager position='top-right' />
            <Outlet />
        </>
    );
};

export default AppRootManager;
