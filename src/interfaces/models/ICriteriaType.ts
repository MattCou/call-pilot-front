export default interface ICriteriaType {
    id: string,
    label: string,
    description: string|null,
    grade: number,
    position: number,
    archivedAt: string|null,
}
